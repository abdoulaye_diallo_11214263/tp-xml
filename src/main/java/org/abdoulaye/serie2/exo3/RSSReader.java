package org.abdoulaye.serie2.exo3;

import java.io.IOException;
import java.io.InputStream;



public class RSSReader {

	public InputStream read(String url) throws ClientProtocolException, IOException {
		// ouverture d'un client
		HttpClient httpClient = HttpClientBuilder.create().build();
		// création d'une méthode HTTP
		HttpGet get = new HttpGet(url);
		HttpResponse response = httpClient.execute(get);
		// invocation de la méthode
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();
		if (statusCode != HttpStatus.SC_OK) {
			System.err.println("Method failed : " + statusLine);
		}
		// lecture de la réponse dans un flux
		InputStream inputStream =  response.getEntity().getContent();

		return inputStream;

	}

}
